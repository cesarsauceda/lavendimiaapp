-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-06-2019 a las 04:52:16
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdvendimia2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `id_articulo` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `modelo` varchar(500) NOT NULL,
  `precio` double NOT NULL,
  `existencia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`id_articulo`, `descripcion`, `modelo`, `precio`, `existencia`) VALUES
(1, 'Base de Cama Matrimonial', 'SANTA LUCIA', 5890, 10),
(2, 'Base de cama King-Size ', 'SANTA LUCIA', 6790, 4),
(3, 'Ropero negro mate', 'Vasconia', 5399, 4),
(4, 'Abanico Myteck', 'PO-20', 1300, 3),
(5, 'Burro para planchar ', 'Azucena 16', 790, 6),
(6, 'Comedor 4 sillas', 'Imperial', 3800, 3),
(7, 'Mesa tablón 2.81 mts', 'Monterrey', 2590, 11),
(8, 'Bateria de peltre 10 pzas', 'Huasteca-SINSA', 1900, 5),
(9, 'Bateria negra 12 Pzas', 'Areli', 2100, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido_paterno` varchar(50) NOT NULL,
  `apellido_materno` varchar(50) NOT NULL,
  `rfc` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `nombre`, `apellido_paterno`, `apellido_materno`, `rfc`) VALUES
(1, 'José Luis', 'Armenta', 'Casas', 'JSC120990JH1'),
(2, 'Martín Jazhiel', 'Robles', 'Pérez', 'RPM220780HG9'),
(3, 'Samuel ', 'Pérez', 'Díaz', 'PDS120498JH0'),
(4, 'Esau', 'Pérez', 'Martinez', 'PME210200KJ8'),
(5, 'Mario', 'Vizcarra', 'Moya', 'MVM210480JLC'),
(6, 'Victor Manuel', 'Reyez', 'Quintero', 'QRV191202XNO'),
(7, 'Amayru', 'Sánchez', 'Camacho', 'CSA120297UHA'),
(8, 'Susana', 'Pérez', 'Salazar', 'SPS111068VPT'),
(9, 'Lucero Antonia', 'Sauceda ', 'Pérez', 'SAPL031092LML'),
(10, 'Amber', 'Espinoza', 'Sauceda', 'SEA160617MAC'),
(11, 'Aurelia', 'López', 'Montes', 'MLA121298MVC');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id_configuracion` int(11) NOT NULL,
  `tasa` double NOT NULL,
  `enganche` double NOT NULL,
  `plazo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id_configuracion`, `tasa`, `enganche`, `plazo`) VALUES
(1, 2.8, 20, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `folio` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `plazos` int(11) NOT NULL,
  `total` double NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`folio`, `id_cliente`, `plazos`, `total`, `fecha`) VALUES
(1, 2, 6, 1272.73, '2019-06-04 04:07:35'),
(2, 2, 6, 5285.73, '2019-06-04 04:18:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas_articulos`
--

CREATE TABLE `ventas_articulos` (
  `id_venta_articulo` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `id_articulo` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventas_articulos`
--

INSERT INTO `ventas_articulos` (`id_venta_articulo`, `id_venta`, `id_articulo`, `cantidad`) VALUES
(1, 1, 4, 1),
(2, 2, 3, 1),
(3, 2, 4, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`id_articulo`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id_configuracion`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`folio`),
  ADD KEY `fk_ventas_clientes` (`id_cliente`) USING BTREE;

--
-- Indices de la tabla `ventas_articulos`
--
ALTER TABLE `ventas_articulos`
  ADD PRIMARY KEY (`id_venta_articulo`),
  ADD KEY `fk_ventas_articulos_articulos` (`id_articulo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `id_articulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `id_configuracion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `folio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `ventas_articulos`
--
ALTER TABLE `ventas_articulos`
  MODIFY `id_venta_articulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `fk_ventas_clientes` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
