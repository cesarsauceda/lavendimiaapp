Aplicación WEB Para el registro de ventas

En la ventana principal se mostrarán todas las ventas que hayan sido registradas
AL dar click sobre el botón nueva venta se mostrará un modal de captura donde se muestra el folio actual de la venta, deberá seleccionar un cliente,
Y un articulo estos campos son obligatorios, una vez seleccionó el cliente y el articulo presionará el botón Agregar, si no existen Articulos en existencia
Se desplegará un mensaje, de lo contrario la agregará a un grid en el cual deberá agregar la cantidad del articulo de venta, no puede seleccionar más
articulos de los que hay en existencia, cuando haya seleccionado la cantidad, se harán los calculos de enganche,total,bonificacion de enganche, importe, 
y le habilitará el botón siguiente, además puede añadir mas artículosl, Una vez presionado el botón siguiente el sistema le mostrará al final del modal
los totales de los plazos a 3,6,9 y 12 meses, de los cuales deberá seleccionar uno para poder habilitar el guardado.
Al presionar el botón guardar el sistema deberá mostrarle un mensaje con exito y se quitará el modal mostrando en la pantalla de inicio la venta generada

ESTRUCTURA DEL PROYECTO:

LaVendimiaAppTest
├─ api/         --->>Back-end
├─── base de datos script/
├────── bdvendimia2.sql   --->>Cargue este script en su localhost
├─── config/
├────── database.php        --->> Conexion a la bd user ="root", pass="root"
├─── controllers/           --->>Carpeta que contiene los controladores que conectan los objetos(Modelos)
├────── articulos/
├─────── getArticulos.php
├────── clientes/
├─────── getClientes.php
├────── configuracion/
├─────── getAConfiguraciones.php
├────── ventas/
├─────── getVentas.php
├─────── getMaxFolio.php
├─────── postVenta.php
├─────── postVentaArticulo.php
├─── models/            --->>COntiene los objetos de las clases, donde se realizan las peticiones a la bd
├────── articulos.php
├────── clientes.php 
├────── configuracion.php 
├────── ventaArticulos.php 
├────── ventas.php 
├─app/          ---->> Front end 
├────assets/ 
├───────css/ --->Estilos
├──────────custom.css
├─── controllers/     --->>Controladores de las vistas
├────── articulosControler.js 
├────── clientesControler.js 
├────── index.js 
├────── ventasControler.js 
├─── factory/     --->>Fabricas de peticiones al backend
├────── articulosFactory.js 
├────── clientesFactory.js 
├────── configuracioFactory.js 
├────── ventasFactory.js 
├─── vistas/     --->>Vistas del proyecto
├───── dialogs/   --->>Modales de venta  
├──────── nueva_venta.html
├──────articulos.html
├──────clientes.html 
├──────ventas.html 
