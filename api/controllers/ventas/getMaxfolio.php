<?php
    // cabeceros requeridos
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    //incluye la base de clase database y el modelo ventas
    include_once '../../config/database.php';
    include_once '../../models/ventas.php';
    
    // instanciamos la conexion de la base de datos 
    $database = new Database();
    $db = $database->getConnection();
    
    // inicializamos objeto ventas
    $ventas = new Ventas($db);
    // query ventas
    $stmt = $ventas->getMaxVenta();
    $num = $stmt->rowCount();

    //Validamos si el objeto nos devolvió información
    if($num>0){
    
        // arreglo de maximo array
        $response=array();
       while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            // extraer el row
            // esto convierte $row['nombre'] a solamente nombre  $nombre 
            extract($row);
            $rw=array(
                "FolioActual" => ($Folio + 1),
                
            );
            array_push($response,$rw);
        }
    
        // enviar codigo de respuesta - 200 OK
        http_response_code(200);
    
        //muestra los datos de los ventas en json
        echo json_encode($response);
    }else{//Si no trajo datos la consulta retornamos eñ error
 
        //enviamos el codigo de respuesta - 404 Not found
        http_response_code(404);
     
        // y le decimos al usuario que no encontró datos
        echo json_encode(
            array("message" => "No se encontraron los ventas.","folioSiguiente" => ($num + 1))
        );
    }
?>
    