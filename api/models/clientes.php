<?php
//Modelo Clientes
    class Clientes{
    
        // conexion bd y nombre tabla
        private $conn;
        private $table_name = "clientes";
    
        // propiedades
        public $id_cliente;
        public $nombre;
        public $apellido_materno;
        public $apellido_paterno;
        public $rfc;
    
        // constructor con $db como conexion 
        public function __construct($db){
            $this->conn = $db;
        }
        // query para traer todos articulos
        function read(){
        
            // Query seleccionar todos los datos
            $query = "SELECT * FROM " . $this->table_name;
        
            // preparamos el query
            $stmt = $this->conn->prepare($query);
        
            // y ejecutamos el query
            $stmt->execute();
        
            //retornamos los datos
            return $stmt;
        }
        function readInd($busqueda){
        
            // Query seleccionar todos los datos
            $query = "SELECT * FROM " . $this->table_name ." WHERE nombre like '%".$busqueda."%' or apellido_materno like '%".$busqueda."%' or apellido_materno like '%".$busqueda."%' or rfc like '%".$busqueda."%'" ;
        
            // preparamos el query
            $stmt = $this->conn->prepare($query);
        
            // y ejecutamos el query
            $stmt->execute();
        
            //retornamos los datos
            return $stmt;
        }
    }
?>