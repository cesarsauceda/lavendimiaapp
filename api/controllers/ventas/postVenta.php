<?php
// cabeceros requeridos
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// conexion
include_once '../../config/database.php';
 
// venta modelo
include_once '../../models/ventas.php';

include_once '../../models/ventaArticulos.php';
 
$database = new Database();
$db = $database->getConnection();
 
$venta = new Ventas($db);
// obtener los datos del request
$data = json_decode(file_get_contents("php://input"));

// asegurar que no venga vacio los parametros
if(
    !empty($data->Parametros->folio) &&
    !empty($data->Parametros->cliente) &&
    !empty($data->Parametros->totalventa) &&
    !empty($data->Parametros->cliente)
){
    // pasamos los valores a las propiedades del objeto
    $venta->id_cliente = $data->Parametros->cliente;
    $venta->total = $data->Parametros->totalventa;
    $venta->plazos = $data->Parametros->plazo;
    $venta->fecha = date('Y-m-d H:i:s');
 
    $ventaArticulo = new VentasArticulos($db);
    
    // insertamos la venta si la inserta insertaremos 
    if($venta->create() && $ventaArticulo->create($data) ){
        
            // regresamos todo bien
            http_response_code(201);
            
            // mensaje respuesta
            echo json_encode(array("message" => "Bien Hecho, Tu venta ha sido registrada correctamente"));
        }else{
            // Error al grabar los articulos de la venta
            http_response_code(503);
    
            echo json_encode(array("message" => "Error al crear el detalle de la venta."));
        }
    
    
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to create venta. Data is incomplete."));
}
?>