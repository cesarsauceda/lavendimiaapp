app.controller('ventasController', ['$scope', '$mdDialog', '$mdToast', '$window', '$timeout', '$mdSidenav', '$rootScope', 'ventasFactory', 'clientesFactory', 'articulosFactory', 'configuracionFactory', function ($scope, $mdDialog, $mdToast, $window, $timeout, $mdSidenav, $rootScope, ventasFactory, clientesFactory, articulosFactory, configuracionFactory) {

    let ventasCtrl = this;
    $scope.articulos = [];
    $scope.clientes = [];
    $scope.ventas = [];
    $scope.configuraciones = [];
    $scope.FolioActual = "";
    $scope.gridActions = {};
    $scope.gridOptions = ventasFactory.getGridOptions();
    $scope.selectedCliente = {};
    $scope.articuloCargado = {};
    $scope.ventasCarrito = [];
    $scope.rfc = "";
    $scope.ExisteArticulos = false;
    $scope.existeTotales = false;
    $scope.selectedId = "";
    $scope.selectedArticulo = "";
    //Parametros de la venta
    $scope.Parametros = {
        folio: "",
        cliente: "",
        plazo: "",
        ventas: $scope.ventasCarrito,
    };

    // cargar articulos 
    $scope.readArticulos = function () {
        articulosFactory.readArticulos().then(function successCallback(response) {
            let articulos = response.data.registros;
            $scope.articulos = articulos;
        }, function errorCallback(response) {
            $scope.showToast(response.message, "error");
        });

    }
    // cargar articulos 
    $scope.readArticuloInd = function (busqueda) {
        if($scope.selectedArticulo.length > 2){
        articulosFactory.readArticulosInd($scope.selectedArticulo).then(function successCallback(response) {
            let articulos = response.data.registros;
            console.log(articulos);
            $scope.articulos = articulos;

        }, function errorCallback(response) {
            $scope.articulos = [];
            console.log("salió");
        });
    }else{
        $scope.articulos = [];
    }

    }
    //$scope.readArticulosInd("Aban");
    //Obtener las configuraciones 
    $scope.readConfiguraciones = function () {
        configuracionFactory.readConfiguraciones().then(function successCallback(response) {
            let configuraciones = response.data.registros;
            $scope.configuraciones = configuraciones;
            let PlazoMaximo = response.data.registros[0];
            $scope.plazosMaximo = PlazoMaximo;
        }, function errorCallback(response) {
            $scope.showToast(response.message, "error");
        });

    }
    //carga los datos del cliente al arreglo
    $scope.cargacliente = function (cliente) {
        $scope.selectedCliente = cliente;
        $scope.selectedId = cliente.id_cliente +" - "+cliente.nombre  +" "+ cliente.apellido_paterno  +" "+ cliente.apellido_materno ;
        $scope.rfc = $scope.selectedCliente.rfc;
        $scope.clientes = [];
    }
    //Articulo seleccionado
    $scope.cargaarticulo = function (articulo) {
        console.log(articulo);
        $scope.articuloCargado = articulo;
        $scope.selectedArticulo = articulo.id_articulo +" - "+articulo.descripcion;
        $scope.articulos = [];
    }
    //Agregar articulos al carrito
    $scope.agregarCarrito = function (articulo) {
        console.log($scope.articuloCargado);
        //Validamos que haya seleccionado un articulo
        if ($scope.articuloCargado.id_articulo > 0) {
            //Parseamos el articulo viene como string

            var obj = $scope.articuloCargado;
            console.log($scope.selectedCliente);
            //Valida que haya seleccionado primero un cliente
            if ( parseInt($scope.selectedCliente.id_cliente) < 1) {
                $scope.showToast("Seleccione un Cliente", "error");
                return;
            } else {
                var cliente = $scope.selectedCliente;
            }

            //buscamos si el articulo ya está en el carrito
            if ($scope.verificarDuplicados(obj.id_articulo)) {
                $scope.showToast("Ya agregó el articulo", "error");
                return;
            }
            //Valida que el articulo seleccionado está en existencia
            if (obj.existencia < 1) {
                $scope.showToast("El articulo seleccionado no cuenta con existencia, favor de validar ", "error");
                return;
            } else {
                obj.cantidad = 0;
                //calcula el precio con intereses
                obj.precio = $scope.calcularPrecio(obj.precio, $scope.plazosMaximo.tasa, $scope.plazosMaximo.plazo);
                obj.total = parseFloat(obj.cantidad * obj.precio).toFixed(2);
                obj.existencia = parseInt(obj.existencia);
                //Agrega el articulo al carrito
                $scope.ventasCarrito.unshift(obj);
                //Limpiamos el select del articulo
                $scope.articuloCargado = {};
                $scope.selectedArticulo = "";
                $scope.articulos = [];
                //Pasamos el folio de la nueva venta y el cliente 
                $scope.Parametros.folio = $scope.FolioActual;
                $scope.Parametros.cliente = cliente.id_cliente;
                $scope.generarTotal();
                if($scope.ventasCarrito.length > 0){
                    $scope.ExisteArticulos = true;
                }else{
                    $scope.ExisteArticulos = false;
                    $scope.existeTotales = false;
                }
            }
        } else {

            $scope.showToast("Seleccione un articulo ", "error");
            return;
        }

    }
    $scope.crearMensualidades = function(){
        for (var i = 0; i < $scope.ventasCarrito.length; i++) {

            if ($scope.ventasCarrito[i]['cantidad'] > $scope.ventasCarrito[i]['existencia'] ) {
                $scope.showToast("El articulo: '"+$scope.ventasCarrito[i]['descripcion']+"' No dispone de la cantidad requerida.", "error");
                return;            
            }else if($scope.ventasCarrito[i]['cantidad'] === undefined || $scope.ventasCarrito[i]['cantidad'] ==="0" ){
                return; 
            }
        }
        $scope.existeTotales = true;
        $scope.Parametros.contado = parseFloat($scope.Parametros.totalventa / ( 1 + (($scope.plazosMaximo.tasa * $scope.plazosMaximo.plazo) / 100 ) )).toFixed(2); 

        $scope.Mensualidades = [{
            'id': 3,
            'meses': '3 PAGOS DE ',
            'abonomensual': $scope.creaAbonoMensual(3),
            'totalpago':+$scope.creaTotalpagar(3),
            'ahorro': $scope.crearAhorro(3)
          }, {
            'id': 6,
            'meses': '6 PAGOS DE ',
            'abonomensual': $scope.creaAbonoMensual(6),
            'totalpago':$scope.creaTotalpagar(6),
            'ahorro': $scope.crearAhorro(6)
          }, {
            'id': 9,
            'meses': '9 PAGOS DE ',
            'abonomensual': $scope.creaAbonoMensual(9),
            'totalpago':+$scope.creaTotalpagar(9),
            'ahorro': $scope.crearAhorro(9)
          }, {
            'id': 12,
            'meses': '12 PAGOS DE ',
            'abonomensual': $scope.creaAbonoMensual(12),
            'totalpago': $scope.creaTotalpagar(12),
            'ahorro': + $scope.crearAhorro(12)
          }];
        

    }

    //Crear TOTAL A PAGAR de 3 6 9 y 12 meses
    $scope.creaTotalpagar = function(meses){
        var aPagar = parseFloat($scope.Parametros.contado * (1 + ($scope.plazosMaximo.tasa * meses) / 100 )).toFixed(2);
        return aPagar; 
    }
    $scope.creaAbonoMensual = function(mes){

        var abono = parseFloat($scope.creaTotalpagar(mes) / mes).toFixed(2);
        return abono;
    }
    $scope.crearAhorro = function(mes){
        var abono = parseFloat($scope.Parametros.totalventa - $scope.creaTotalpagar(mes)).toFixed(2); 
        return abono;
    }
      

  
    //Calcular el precio del articulo ya con intereses con la configuracion
    // que tiene el plazo  más largo en las configuraciones
    $scope.calcularPrecio = function (precio, tasa, plazo) {
        $precio = parseFloat(precio * (1 + ((parseFloat(tasa * plazo)) / 100))).toFixed(2);
        return $precio;

    }

    //Actualizar Importe del Articulo
    $scope.actualizaImporte = function (valor) {
        //Valida si la cantidad no sea mayor que la existencia
        var el = document.getElementById('myInput');
        el.setAttribute("max",valor.existencia);
        if (valor.valor > valor.existencia) {
            $scope.showToast("La cantidad es mayor que la existencia del articulo", "error");
            return;
        }

        //Actualiza el importe del articulo que ya está en el carrito (Agregados al Grid)
        var searchField = "id_articulo";
        var searchVal = valor.id_articulo;
        for (var i = 0; i < $scope.ventasCarrito.length; i++) {
            if ($scope.ventasCarrito[i][searchField] == searchVal) {
                $scope.ventasCarrito[i].importe = parseFloat(valor.cantidad * valor.precio).toFixed(2);
            }
        }
        $scope.generarTotal();

    }
    //Validar que no esté agregado el articulo en el Grid
    $scope.verificarDuplicados = function (id_articulo) {

        var results = false;
        var searchField = "id_articulo";
        var searchVal = id_articulo;
        for (var i = 0; i < $scope.ventasCarrito.length; i++) {
            if ($scope.ventasCarrito[i][searchField] == searchVal) {
                results = true;
            }
        }
        return results;

    }

    //Generar importe total de la venta, enganche
    $scope.generarTotal = function(){
        $scope.Parametros.total = 0;
        var importe = 0;
        for (var i = 0; i < $scope.ventasCarrito.length; i++) {
            importe = parseFloat(parseFloat(importe).toFixed(2) + parseFloat($scope.ventasCarrito[i].importe).toFixed(2)).toFixed(2);
            $scope.Parametros.total = + parseFloat(parseFloat($scope.Parametros.total) + parseFloat($scope.ventasCarrito[i].importe)).toFixed(2);
        }
        $scope.Parametros.enganche = parseFloat(($scope.plazosMaximo.enganche / 100) * $scope.Parametros.total).toFixed(2);
        $scope.Parametros.bonificacion = parseFloat($scope.Parametros.enganche * (($scope.plazosMaximo.tasa * $scope.plazosMaximo.plazo)/100)).toFixed(2);
        $scope.Parametros.totalventa = parseFloat($scope.Parametros.total - $scope.Parametros.enganche - $scope.Parametros.bonificacion).toFixed(2);
      
     //$scope.ventasCarrito[i].importe = parseFloat(valor.cantidad * valor.precio).toFixed(2);

    }
    //Eliminar Articulo
    $scope.eliminarArticulo = function (articulo) {
        var confirm = $mdDialog.confirm()
            .title("Eliminar Articulo")
            .textContent("¿Seguro que desea eliminar el articulo de esta venta '" + articulo.descripcion + "' ?")
            .targetEvent()
            .ok('Aceptar')
            .cancel('Cancelar');

        $mdDialog.show(confirm).then(function () {
            $scope.ventasCarrito.forEach(function (currentValue, index, arr) {
                if ($scope.ventasCarrito[index].id_articulo === articulo.id_articulo) {
                    $scope.ventasCarrito.splice(index, 1);
                }

            })
            $scope.generarTotal();
            $scope.crearMensualidades();
            $scope.showCreateProductForm();
                
                if($scope.ventasCarrito.length > 0){
                    $scope.ExisteArticulos = true;
                }else{
                    $scope.ExisteArticulos = false;
                    $scope.existeTotales = false;
                }
            
        }, function () {
            $scope.generarTotal();
            $scope.showCreateProductForm();
        });

    }

    //Obtener Clientes
    $scope.readClientes = function () {
        // use clientes factory
        clientesFactory.readClientes().then(function successCallback(response) {
            let clientes = response.data.registros;
            $scope.clientes = clientes;
        }, function errorCallback(response) {
            $scope.showToast(response.message, "error");
        });

    }
    $scope.busqueda = ""
     //Obtener Clientes
     $scope.readClientesInd = function () {
         console.log($scope.selectedId);
        // use clientes factory
        if($scope.selectedId.length > 2){
        clientesFactory.readClientesInd($scope.selectedId).then(function successCallback(response) {
            let clientes = response.data.registros;
            $scope.clientes = clientes;
        }, function errorCallback(response) {
            $scope.clientes = [];
        });}
        else{
            $scope.clientes = [];
            $scope.rfc = "";
            
        }

    }
    //$scope.readClientesInd("SAP");

    //Obtener ventas
    $scope.readVentas = function () {

        ventasFactory.readVentas().then(function successCallback(response) {
            let ventas = response.data.registros;
            $scope.ventas = ventas;
            $scope.gridOptions.data = response.data.registros;
        }, function errorCallback(response) {
            $scope.showToast(response.message, "error");
        });

    }
    //Obtener el FOlio actual de la venta
    $scope.readMaxfolio = function () {

        ventasFactory.readMax().then(function successCallback(response) {
            let max = response.data[0].FolioActual;
            $scope.FolioActual = max;
        }, function errorCallback(response) {
            $scope.showToast(response.message, "error");
        });

    }

    //funcion para consumir las ventas,clientes, articulos, Configuraciones y 
    //nuevo folio cuando carga la página
    $scope.main = function () {
        $scope.readVentas();
     //   $scope.readClientes();
        //$scope.readArticulos();
        $scope.readMaxfolio();
        $scope.readConfiguraciones();
        $scope.selectedCliente = {};
        $scope.articuloCargado = {};
        $scope.ventasCarrito = [];
        $scope.rfc = "";
        $scope.ExisteArticulos = false;
        $scope.existeTotales = false;
        $scope.selectedId = "";
        $scope.saving = false;
        //Parametros de la venta
        $scope.Parametros = {
            folio: "",
            cliente: "",
            plazo: "",
            ventas: $scope.ventasCarrito,
        };

    }
    // show toast message
    $scope.showToast = function (message, status) {
        $mdToast.show({
            template: '<md-toast class="md-toast.' + status + '">' + message + '</md-toast>',
            hideDelay: 2000,
            position: 'top right'
        });

    }
    $scope.main();

    //Abrir modal de nueva venta
    $scope.showCreateProductForm = function (event) {

        $mdDialog.show({
            controller: DialogController,
            templateUrl: './vistas/dialogs/nueva_venta.html',
            parent: angular.element(document.body),
            clickOutsideToClose: false,
            scope: $scope,
            preserveScope: true,
            fullscreen: true
        });
    }

    //Controlador del dialog
    function DialogController($scope, $mdDialog) {
        $scope.cancel = function () {

            var confirm = $mdDialog.confirm()
            .title("Cancelar Venta")
            .textContent("¿Seguro que desea cancelar la venta?")
            .targetEvent()
            .ok('Aceptar')
            .cancel('Cancelar');

            $mdDialog.show(confirm).then(function () {
                $mdDialog.cancel();
                $scope.main();
            }, function () {
                $scope.showCreateProductForm();
            });
        };
        $scope.guardar = function() {
            $scope.saving = true;
            if($scope.Parametros.plazo === "" || $scope.Parametros.plazo === undefined || $scope.Parametros.plazo === null){
                $scope.showToast("Debe seleccionar un plazo para realizar el pago de su compra","error");
                return;
            }
            ventasFactory.postVenta($scope).then(function successCallback(response) {
                $scope.showToast(response.data.message, "success");
                $mdDialog.cancel();

                $scope.main();
    


            }, function errorCallback(response) {
                $scope.saving = false;
                $scope.showToast("Error al guardar la venta", "error");
            });

        }
    }

}]);