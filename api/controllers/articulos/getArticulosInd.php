<?php
    // cabeceros requeridos
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
     
    //incluye la base de clase database y el modelo articulos
    include_once '../../config/database.php';
    include_once '../../models/articulos.php';
    
    // instanciamos la conexion de la base de datos 
    $database = new Database();
    $db = $database->getConnection();

   // obtener los datos del request
$data = json_decode(file_get_contents("php://input"));
    // inicializamos objeto articulo
    $articulo = new Articulos($db);
    // query articulos
    $stmt = $articulo->readInd($data[0]);
    $num = $stmt->rowCount();
    
    //Validamos si el objeto nos devolvió información
    if($num>0){
    
        // arreglo de articulos array
        $articulos_arr=array();
        $articulos_arr["registros"]=array();
    
       while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            // extraer el row
            // esto convierte $row['nombre'] a solamente nombre  $nombre 
            extract($row);
    
            $articulos_item=array(
                "id_articulo" => $id_articulo,
                "descripcion" => $descripcion,
                "modelo" => html_entity_decode($modelo),
                "precio" => $precio,
                "existencia" => $existencia,
            );
    
            array_push($articulos_arr["registros"], $articulos_item);
        }
    
        // enviar codigo de respuesta - 200 OK
        http_response_code(200);
    
        //muestra los datos de los articulos en json
        echo json_encode($articulos_arr);
    }else{//Si no trajo datos la consulta retornamos eñ error
 
        //enviamos el codigo de respuesta - 404 Not found
        http_response_code(201);
     
        // y le decimos al usuario que no encontró datos
        echo json_encode(
            array("message" => "Nada Relacionado")
        );
    }
    