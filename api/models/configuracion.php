<?php
//Modelo Configuraciones
    class Configuraciones{
    
        // conexion bd y nombre tabla
        private $conn;
        private $table_name = "configuracion";
    
        // propiedades
        public $id_configuracion;
        public $plazo;
        public $tasa;
        public $enganche;
    
        // constructor con $db como conexion 
        public function __construct($db){
            $this->conn = $db;
        }
        // query para traer todos articulos
        function read(){
        
            // Query seleccionar todos los datos
            $query = "SELECT * FROM " . $this->table_name." ORDER BY plazo DESC";
        
            // preparamos el query
            $stmt = $this->conn->prepare($query);
        
            // y ejecutamos el query
            $stmt->execute();
        
            //retornamos los datos
            return $stmt;
        }
    }
?>