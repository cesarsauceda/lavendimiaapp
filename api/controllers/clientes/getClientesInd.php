<?php
    // cabeceros requeridos
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
     
    //incluye la base de clase database y el modelo clientes
    include_once '../../config/database.php';
    include_once '../../models/clientes.php';
    
    // instanciamos la conexion de la base de datos 
    $database = new Database();
    $db = $database->getConnection();

   // obtener los datos del request
$data = json_decode(file_get_contents("php://input"));
    // inicializamos objeto cliente
    $cliente = new Clientes($db);
    // query clientes
    $stmt = $cliente->readInd($data[0]);
    $num = $stmt->rowCount();
    //Validamos si el objeto nos devolvió información
  
    //Validamos si el objeto nos devolvió información
    if($num>0){
    
        // arreglo de clientes array
        $clientes_arr=array();
        $clientes_arr["registros"]=array();
    
       while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            // extraer el row
            // esto convierte $row['nombre'] a solamente nombre  $nombre 
            extract($row);
    
            $clientes_item=array(
                "id_cliente" => $id_cliente,
                "nombre" => $nombre,
                "apellido_materno" => $apellido_materno,
                "apellido_paterno" => $apellido_paterno,
                "rfc" => $rfc,
            );
    
            array_push($clientes_arr["registros"], $clientes_item);
        }
    
        // enviar codigo de respuesta - 200 OK
        http_response_code(200);
    
        //muestra los datos de los clientes en json
        echo json_encode($clientes_arr);
    }else{//Si no trajo datos la consulta retornamos eñ error
 
        //enviamos el codigo de respuesta - 404 Not found
        http_response_code(404);
     
        // y le decimos al usuario que no encontró datos
        echo json_encode(
            array("message" => "No se encontraron los clientes.")
        );
    }
?>
    