<?php
//Modelo Ventas
    class VentasArticulos{
    
        // conexion bd y nombre tabla
        private $conn;
        private $table_name = "ventas_articulos";
    
        // propiedades
        public $id_venta_articulo;
        public $id_venta;
        public $id_articulo;
        public $cantidad;
    
        // constructor con $db como conexion 
        public function __construct($db){
            $this->conn = $db;
        }

        //agregar Venta
        function create($data){
            $actualizo = true;
            // query para insertar en venta
            foreach($data->Parametros->ventas as $venta){
                $query = "INSERT INTO
                " . $this->table_name . "
                        SET id_venta=".$data->Parametros->folio.", id_articulo=".$venta->id_articulo.", cantidad=".$venta->cantidad;

                
                $stmt = $this->conn->prepare($query);
                if($stmt->execute()){
                    $query2 = "UPDATE articulos set existencia = existencia - ".($venta->cantidad)." where id_articulo=".$venta->id_articulo;
                    $stmt = $this->conn->prepare($query2);
                    if($stmt->execute()){
                        $actualizo = true;
                    }else{
                        $actualizo = false;
                    }
                }else{
                    $actualizo = false;
                }
            }
            return $actualizo;
        
            // $this->id_venta=htmlspecialchars(strip_tags($this->id_venta));
            // $this->id_articulo=htmlspecialchars(strip_tags($this->id_articulo));
            // $this->cantidad=htmlspecialchars(strip_tags($this->cantidad));
            // // valores
            // $stmt->bindParam(":id_venta", $this->id_venta);
            // $stmt->bindParam(":id_articulo", $this->id_articulo);
            // $stmt->bindParam(":cantidad", $this->cantidad);
            // // execute query
            // if($stmt->execute()){
            //     $query2 = "UPDATE articulos set existencia = existencia - ".($this->cantidad)." where id_articulo=".$this->id_articulo;
            //     $stmt = $this->conn->prepare($query2);
            //     if($stmt->execute()){
            //         return true;
            //     }else{
            //         echo "No insertó";
            //     }
            // }
        
            
        }
    }
?>