<?php
//Modelo Ventas
    class Ventas{
    
        // conexion bd y nombre tabla
        private $conn;
        private $table_name = "ventas";
    
        // propiedades
        public $id_venta;
        public $plazos;
        public $enganche;
        public $id_cliente;
        public $total;
        public $fecha;
    
        // constructor con $db como conexion 
        public function __construct($db){
            $this->conn = $db;
        }
        // query para traer todas las ventas
        function read(){
        
            // Query seleccionar todos los datos
            $query = "SELECT 
					v.folio, v.id_cliente , c.nombre, c.apellido_paterno, c.apellido_materno , v.total, v.fecha
				FROM ". $this->table_name." v
					
				INNER JOIN clientes c
				
				ON v.id_cliente = c.id_cliente ORDER BY fecha DESC";
        
            // preparamos el query
            $stmt = $this->conn->prepare($query);
        
            // y ejecutamos el query
            $stmt->execute();
        
            //retornamos los datos
            return $stmt;
        }

        function getMaxVenta(){
        
            // Query seleccionar todos los datos
            $query = "SELECT MAX(folio) AS Folio
				FROM 
				ventas
			";
        
            // preparamos el query
            $stmt = $this->conn->prepare($query);
        
            // y ejecutamos el query
            $stmt->execute();
        
            //retornamos los datos
            return $stmt;
        }

        //agregar Venta
        function create(){
        
            // query para insertar en venta
            $query = "INSERT INTO
                        " . $this->table_name . "
                    SET
                        id_cliente=:id_cliente, plazos=:plazos, total=:total, fecha=:fecha";
        
            
            $stmt = $this->conn->prepare($query);
        
            $this->id_cliente=htmlspecialchars(strip_tags($this->id_cliente));
            $this->plazos=htmlspecialchars(strip_tags($this->plazos));
            $this->total=htmlspecialchars(strip_tags($this->total));
            $this->fecha=htmlspecialchars(strip_tags($this->fecha));
            $hoy = date("Y-m-d H:i:s");             
            // valores
            $stmt->bindParam(":id_cliente", $this->id_cliente);
            $stmt->bindParam(":plazos", $this->plazos);
            $stmt->bindParam(":total", $this->total);
            $stmt->bindParam(":fecha", $this->fecha);
            // execute query
            if($stmt->execute()){
                return true;
            }
        
            return false;
            
        }
    }
?>