<?php
    // cabeceros requeridos
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    //incluye la base de clase database y el modelo ventas
    include_once '../../config/database.php';
    include_once '../../models/ventas.php';
    
    // instanciamos la conexion de la base de datos 
    $database = new Database();
    $db = $database->getConnection();
    
    // inicializamos objeto ventas
    $ventas = new Ventas($db);
    // query ventas
    $stmt = $ventas->read();
    $num = $stmt->rowCount();

    
    //Validamos si el objeto nos devolvió información
    if($num>0){
    
        // arreglo de ventas array
        $ventas_arr=array();
        $ventas_arr["registros"]=array();
       while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            // extraer el row
            // esto convierte $row['nombre'] a solamente nombre  $nombre 
            extract($row);
            $date = date_create($fecha);
            $fecha = date_format($date, 'd/m/Y');

            $ventas_item=array(
                "folio" => $folio,
                "id_cliente" => $id_cliente,
                "nombre" => $nombre,
                "apellido_paterno" => $apellido_paterno,
                "apellido_materno" => $apellido_materno,
                "total" => $total,
                "fecha" => $fecha,
                
            );
            array_push($ventas_arr["registros"], $ventas_item);

        }
    
        // enviar codigo de respuesta - 200 OK
        http_response_code(200);
    
        //muestra los datos de los ventas en json
        echo json_encode($ventas_arr);
    }else{//Si no trajo datos la consulta retornamos eñ error
 
        //enviamos el codigo de respuesta - 404 Not found
        http_response_code(201);
     
        // y le decimos al usuario que no encontró datos
        echo json_encode(
            array("message" => "No existe ninguna venta aun.","folioSiguiente" => ($num + 1))
        );
    }
?>
    