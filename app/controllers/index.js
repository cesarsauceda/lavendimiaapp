$(document).ready(function(){

	var link = "";
	
	$('.navbar').on('click','.dropdown-item',function(){
		
		var file_name = $(this).data('link');
		
		switch(file_name){
			case 'inicio':
				cargaCuerpo("index"+link);
				break;
			case 'ventas':
				link = "ventas";
				break;
			case 'clientes':
				link = "clientes";
				break;
			case 'articulos':
				link  = "articulos";
				break;
			case 4:
				link = "configuracion";    /*nombre de las vistas*/
				break;
		}		
		
		cargaCuerpo("vistas/"+link);
		
	});
	
fecha();
	
	
});

function cargaCuerpo(link){
	$('#cuerpo').load(link +'.html');
}	

function fecha(){				
var d = new Date();

var month = d.getMonth()+1;
var day = d.getDate();

var output = 'Fecha: ' + d.getFullYear() + '/' +
    (month<10 ? '0' : '') + month + '/' +
    (day<10 ? '0' : '') + day;
	
	$('#fecha').html(output);
}