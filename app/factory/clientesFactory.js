app.factory("clientesFactory", function($http){
 
    var factory = {};
 
    function getHeaders() {
        return {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept': 'application/json',
            'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
            'Access-Control-Allow-Origin': '*'
        }
    }

    // read all clientes
    factory.readClientes = function(){
        return $http({
            method: 'GET',
            url: 'http://localhost/LaVendimiaAppTest/api/controllers/clientes/getClientes.php'
        });
    }
    factory.readClientesInd = function(busqueda){
        console.log("Entro");
        return $http({
            method: 'POST',
            data: [busqueda],
            url: 'http://localhost/LaVendimiaAppTest/api/controllers/clientes/getClientesInd.php',
            headers: getHeaders(),

        });
    };
     
    return factory;
});