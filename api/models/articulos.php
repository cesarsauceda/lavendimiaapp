<?php
//Modelo Articulos
    class Articulos{
    
        // conexion bd y nombre tabla
        private $conn;
        private $table_name = "articulos";
    
        // propiedades
        public $id_articulo;
        public $descripcion;
        public $modelo;
        public $precio;
        public $existencia;
    
        // constructor con $db como conexion 
        public function __construct($db){
            $this->conn = $db;
        }
        // query para traer todos articulos
        function read(){
        
            // Query seleccionar todos los datos
            $query = "SELECT * FROM " . $this->table_name;
        
            // preparamos el query
            $stmt = $this->conn->prepare($query);
        
            // y ejecutamos el query
            $stmt->execute();
        
            //retornamos los datos
            return $stmt;
        }
        function readInd($busqueda){
        
            // Query seleccionar todos los datos
            $query = "SELECT * FROM " . $this->table_name ." WHERE descripcion like '%".$busqueda."%'" ;
        
            // preparamos el query
            $stmt = $this->conn->prepare($query);
        
            // y ejecutamos el query
            $stmt->execute();
        
            //retornamos los datos
            return $stmt;
        }
    }
?>