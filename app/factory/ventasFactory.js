app.factory("ventasFactory", function ($http) {

    var factory = {};

    
    function getHeaders() {
        return {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept': 'application/json',
            'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
            'Access-Control-Allow-Origin': '*'
        }
    }

    // leer folio actual
    factory.readMax = function () {
        return $http({
            method: 'GET',
            url: 'http://localhost/LaVendimiaAppTest/api/controllers/ventas/getMaxfolio.php'
        });
    };

    // leer todas las ventas
    factory.readVentas = function () {
        return $http({
            method: 'GET',
            url: 'http://localhost/LaVendimiaAppTest/api/controllers/ventas/getVentas.php'
        });
    };

    factory.postVenta = function($scope){
        return $http({
            method: 'POST',
            data: {
                'Parametros' : $scope.Parametros,
            },
            url: 'http://localhost/LaVendimiaAppTest/api/controllers/ventas/postVenta.php',
            headers: getHeaders(),
            
            transformResponse: function (x) {
                return angular.fromJson(angular.fromJson(x));
            }
        });
    };
    
    factory.postVentaArticulo = function($scope){
        return $http({
            method: 'POST',
            data: {
                'Parametros' : $scope.Parametros,
            },
            url: 'http://localhost/LaVendimiaAppTest/api/controllers/ventas/postVentaArticulo.php'
        });
    };

    factory.getGridOptions = function () {
        return {
            data: []
        };
    };
    return factory;
});