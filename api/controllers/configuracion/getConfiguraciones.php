<?php
    // cabeceros requeridos
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    //incluye la base de clase database y el modelo configuraciones
    include_once '../../config/database.php';
    include_once '../../models/configuracion.php';
    
    // instanciamos la conexion de la base de datos 
    $database = new Database();
    $db = $database->getConnection();
    
    // inicializamos objeto configuracion
    $configuracion = new Configuraciones($db);
    // query configuraciones
    $stmt = $configuracion->read();
    $num = $stmt->rowCount();
    
    //Validamos si el objeto nos devolvió información
    if($num>0){
    
        // arreglo de configuraciones array
        $configuraciones_arr=array();
        $configuraciones_arr["registros"]=array();
    
       while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            // extraer el row
            // esto convierte $row['nombre'] a solamente nombre  $nombre 
            extract($row);
    
            $configuraciones_item=array(
                "id_configuracion" => $id_configuracion,
                "enganche" => $enganche,
                "tasa" => $tasa,
                "plazo" => $plazo,
            );
    
            array_push($configuraciones_arr["registros"], $configuraciones_item);
        }
    
        // enviar codigo de respuesta - 200 OK
        http_response_code(200);
    
        //muestra los datos de las configuraciones en json
        echo json_encode($configuraciones_arr);
    }else{//Si no trajo datos la consulta retornamos eñ error
 
        //enviamos el codigo de respuesta - 404 Not found
        http_response_code(404);
     
        // y le decimos al usuario que no encontró datos
        echo json_encode(
            array("message" => "No se encontraron las configuraciones.")
        );
    }
?>
    