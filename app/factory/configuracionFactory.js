app.factory("configuracionFactory", function($http){
 
    var factory = {};
 
    // read all configuracion
    factory.readConfiguraciones = function(){
        return $http({
            method: 'GET',
            url: 'http://localhost/LaVendimiaAppTest/api/controllers/configuracion/getConfiguraciones.php'
        });
    };
     
     
    return factory;
});